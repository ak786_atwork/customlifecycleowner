package com.example.lifecycleownerdemo

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

class CustomLifeCycleOwner : LifecycleOwner{

    private var mLifecycleRegistry:LifecycleRegistry

    constructor() {
        mLifecycleRegistry = LifecycleRegistry(this)

        val customComponent = CustomComponent()
        lifecycle.addObserver(customComponent)

        mLifecycleRegistry.markState(Lifecycle.State.INITIALIZED)
        mLifecycleRegistry.markState(Lifecycle.State.CREATED)
        mLifecycleRegistry.markState(Lifecycle.State.STARTED)
        mLifecycleRegistry.markState(Lifecycle.State.RESUMED)
        mLifecycleRegistry.markState(Lifecycle.State.DESTROYED)
    }

    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry
    }
}