package com.example.lifecycleownerdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CpuUsageInfo
import android.util.Log

class MainActivity : AppCompatActivity() {

    val TAG = "COMPONENT_LC"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val customLifeCycleOwner = CustomLifeCycleOwner()
//        val customComponent = CustomComponent()
//        lifecycle.addObserver(customComponent)

        showLog("oncreate called")
    }

    override fun onStart() {
        super.onStart()
        showLog("onstart called")
    }

    override fun onResume() {
        super.onResume()
        showLog("onresume called")
    }

    override fun onPause() {
        super.onPause()
        showLog("onpause called")
    }

    override fun onStop() {
        super.onStop()

        showLog("onstop called")
    }

    override fun onDestroy() {
        super.onDestroy()

        showLog("ondestroy")
    }



    private fun showLog(s: String) {
//        Log.d(TAG, "activity "+s)
    }
}
