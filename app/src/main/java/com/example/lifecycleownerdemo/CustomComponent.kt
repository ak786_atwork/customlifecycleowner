package com.example.lifecycleownerdemo

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class CustomComponent : LifecycleObserver {

    val TAG = "COMPONENT_LC"

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public fun oncreateEvent() {
        showLog("oncreate called")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public fun onstartEvent() {
        showLog("onstart called")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public fun onpauseEvent() {
        showLog("onpause called")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public fun onresumeEvent() {
        showLog("onresume called")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public fun onstopEvent() {
        showLog("onstop called")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public fun ondestroyEvent() {
        showLog("ondestroy called")
    }


    fun showLog(info: String) {
        Log.d(TAG, "CustomComponent "+info)
    }

}




/**
* result:
 *
 * 2020-05-01 09:20:19.953 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity oncreate called
2020-05-01 09:20:19.976 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent oncreate called
2020-05-01 09:20:19.986 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity onstart called
2020-05-01 09:20:20.011 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent onstart called
2020-05-01 09:20:20.011 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity onresume called
2020-05-01 09:20:20.051 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent onresume called
2020-05-01 09:20:52.433 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent onpause called
2020-05-01 09:20:52.434 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity onpause called
2020-05-01 09:20:53.572 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent onstop called
2020-05-01 09:20:53.585 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity onstop called
2020-05-01 09:20:53.591 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: CustomComponent ondestroy called
2020-05-01 09:20:53.591 26132-26132/com.example.lifecycleownerdemo D/COMPONENT_LC: activity ondestroy
*
*
* */